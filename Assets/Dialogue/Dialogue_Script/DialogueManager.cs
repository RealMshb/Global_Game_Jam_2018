using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour {

    public static DialogueManager Instance;

	public Text nameText;
	public Text dialogueText;


	private Queue<string> sentences;
	private Queue<string> copySentences;


	private Dialogue copyDialogue;
	private string objectName;
    void Awake()
    {
        Instance = this;
    }

	// Use this for initialization
	void Start () {
		sentences = new Queue<string>();
		copySentences = new Queue<string>();
	}

	
	public void StartDialogue (Dialogue dialogue )
	{
		//print("Start Dialogue");
	
		copyDialogue = dialogue;
		copySentences = sentences;
	
		nameText.text = dialogue.name;

		if(sentences != null)
		{
			sentences.Clear();
			foreach (string sentence in dialogue.sentences)
			{
				sentences.Enqueue(sentence);
			}
			DisplayNextSentence();
		}
		else
		{
			foreach (string sentence in dialogue.sentences)
			{
				sentences.Enqueue(sentence);
			}
			DisplayNextSentence();
		
		}


	}

	public void DisplayNextSentence ()
	{
		if (sentences.Count == 0)
		{
		
			EndDialogue();
			StartDialogue(copyDialogue);
			return;
		}

		string sentence = sentences.Dequeue();
		StopAllCoroutines();
		StartCoroutine(TypeSentence(sentence));
	}

	IEnumerator TypeSentence (string sentence)
	{
		dialogueText.text = "";
		foreach (char letter in sentence.ToCharArray())
		{
			dialogueText.text += letter;
			yield return null;
		}
	}

	void EndDialogue()
	{
		//animator.SetBool("IsOpen", false);
		sentences = copySentences;
	}

}
