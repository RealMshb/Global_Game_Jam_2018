﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInnerThoughts : MonoBehaviour {

	public GameObject targetCharacter;
	public DialogueTrigger thoughts;

	public void setTargetCharacter(GameObject selectedCharacter)
	{
		targetCharacter = selectedCharacter;
	}

	public void setDialogue(DialogueTrigger selectedThoughts)
	{
		thoughts = selectedThoughts;
	}


}
