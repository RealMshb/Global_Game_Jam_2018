﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    public Transform target;
    public Vector3 offset;
    public float smoothTime = 0.3F;

    private Vector3 velocity = Vector3.zero;
	
	// Update is called once per frame
    bool Pressed= false;
	void Update () {
        if(Input.GetButton("start")&& !Pressed)
        {
             
                Pressed = true;
        }

        if(Pressed){
            Vector3 targetPosition = target.position + offset;
              transform.position = Vector3.SmoothDamp(transform.position, targetPosition ,ref velocity,smoothTime);
        }
       
	}

    public void newTarget(Transform targetSelected){
        target = targetSelected;
    }
}
