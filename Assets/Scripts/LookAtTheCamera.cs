﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtTheCamera : MonoBehaviour {

    private Transform target;

	// Update is called once per frame
    void Start(){
        target = Camera.main.transform;
    }
	void Update () {
        transform.LookAt(transform.position + target.rotation * Vector3.forward,
            target.rotation * Vector3.up);
    }
}
