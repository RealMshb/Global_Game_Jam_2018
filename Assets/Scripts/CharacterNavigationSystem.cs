﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;


public class CharacterNavigationSystem : MonoBehaviour
{

    [System.Serializable]
    public struct WalkNode
    {
        public Transform nodeTransform;
        public string speech;
        public bool speechAtThisNode;
        public float WaitTime;
    }

    public WalkNode[] nodes;
    public GameObject speechBubble;
    public Text speechBubbleText;

    private NavMeshAgent agent;
    private int nodeID = 0;
    private Coroutine waitInThisNodeCoroutine;
    private Coroutine checkForArriveCoroutine;
    private Coroutine WriteTheSpeechCoroutine;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        WalkToNextNode();
        speechBubble.SetActive(false);
    }

    void WalkToNextNode()
    { 
        Debug.Log("Started to walk to node " + nodeID);
        if (waitInThisNodeCoroutine != null)
            StopCoroutine(waitInThisNodeCoroutine);

        agent.SetDestination(nodes[nodeID].nodeTransform.position);
        checkForArriveCoroutine = StartCoroutine(CheckForArrive());
    }

    IEnumerator WaitInThisNode()
    {
        Debug.Log("Wait for "+ nodes[nodeID].WaitTime + " seconeds in node " + nodeID);
        if(nodes[nodeID].speechAtThisNode)
        {
            speechBubble.SetActive(true);
            WriteTheSpeechCoroutine = StartCoroutine(WriteTheSpeech());
        }

        yield return new WaitForSeconds(nodes[nodeID].WaitTime);

        if (nodeID < nodes.Length - 1)
            nodeID++;
        else
            nodeID = 0;

        speechBubble.SetActive(false);
        WalkToNextNode();
    }

    IEnumerator CheckForArrive()
    {
        while (transform.position.x != nodes[nodeID].nodeTransform.position.x && transform.position.z != nodes[nodeID].nodeTransform.position.z)
        {
            yield return null;
        }

        Debug.Log("Arrived at node " + nodeID);
        waitInThisNodeCoroutine = StartCoroutine(WaitInThisNode());
        StopCoroutine(checkForArriveCoroutine);
    }

    IEnumerator WriteTheSpeech()
    {
        int count = 0;
        speechBubbleText.text = "";
        while (count < nodes[nodeID].speech.Length)
        {
            speechBubbleText.text += nodes[nodeID].speech[count];
            yield return new WaitForFixedUpdate();
            count++;
        }
        StopCoroutine(WriteTheSpeechCoroutine);
    }


}
