﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchCharacter : MonoBehaviour
{
    public Transform transitionEffect;
    public GameObject firstCharacter;
    public float transitionTime;
    RaycastHit hit;
    public float smoothTime = 0.3F;

    private Vector3 velocity = Vector3.zero;
    private Coroutine transitionEffectMoverCoroutine;
    private Coroutine transitionEffectDeactiveCoroutine;

    private Transform privousObject;

      Transform selectedCharacter;
    // Use this for initialization
    void Start()
    {

       
       // Invoke("InititiateFirstCharacter",1f);
    }

    public void InititiateFirstCharacter()
    {
        this.GetComponent<CameraMovement>().newTarget(firstCharacter.transform);
       firstCharacter.GetComponent<AudioSource>().Play();
        firstCharacter.GetComponent<DialogueTrigger>().TriggerDialogue();
       privousObject = firstCharacter.transform;
    }

    // Update is called once per frame
    void Update()
    {

        if(Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }


        if (Input.GetMouseButtonDown(0) && startGame.Instance.isPressed)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag != "IgnoreCollider")
                {
                    if(hit.transform.tag != "NotSwitchable")
                    {
                        if(  privousObject.gameObject.GetComponent<AudioSource>() != null){
                             privousObject.gameObject.GetComponent<AudioSource>().Stop();
                        }
                       
                        if (transitionEffectMoverCoroutine != null)
                            StopCoroutine(transitionEffectMoverCoroutine);
                        if (transitionEffectDeactiveCoroutine != null)
                            StopCoroutine(transitionEffectDeactiveCoroutine);

                        transitionEffect.gameObject.SetActive(false);

                        print("Selected Target: " + hit.transform.tag);

                        if (privousObject != null)
                            transitionEffect.position = privousObject.position;

                        selectedCharacter= hit.transform;
                        privousObject = selectedCharacter;
                        transitionEffect.gameObject.SetActive(true);
                        this.GetComponent<CameraMovement>().newTarget(selectedCharacter);
                        transitionEffectMoverCoroutine = StartCoroutine(TransitionEffectMover(selectedCharacter));
                      
                       
                        if(selectedCharacter.gameObject.GetComponent<AudioSource>()!= null)
                        {
                        
                             selectedCharacter.gameObject.GetComponent<AudioSource>().Play();
                        }

                       
                        
                        setInnerThoughtsDialogue();
                        startDialogue();
                    }
                    
                    

                }
                else
                {
                    DialogueManager.Instance.DisplayNextSentence();
                }
            }
        }

    }

    void setInnerThoughtsDialogue()
    {
        //GameObject.Find("DialogueManager").GetComponent<CharacterInnerThoughts>().setTargetCharacter(hit.transform.gameObject);
        //GameObject.Find("DialogueManager").GetComponent<CharacterInnerThoughts>().setDialogue();
    }

    void startDialogue()
    {
        if (hit.transform.gameObject.GetComponent<DialogueTrigger>())
            hit.transform.gameObject.GetComponent<DialogueTrigger>().TriggerDialogue();
    }

    IEnumerator TransitionEffectMover(Transform otherTransform)
    {
        transitionEffectDeactiveCoroutine = StartCoroutine(transitionEffectDeactive());
        while (transitionEffect.position.x != otherTransform.position.x && transitionEffect.position.z != otherTransform.position.z)
        {
            transitionEffect.position = Vector3.SmoothDamp(transitionEffect.position, otherTransform.position, ref velocity, smoothTime);
            yield return null;
        }


    }

    IEnumerator transitionEffectDeactive()
    {
        yield return new WaitForSeconds(transitionTime);
        if (transitionEffectMoverCoroutine != null)
            StopCoroutine(transitionEffectMoverCoroutine);
        transitionEffect.gameObject.SetActive(false);
    }
}
