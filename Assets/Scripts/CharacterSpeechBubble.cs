﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSpeechBubble : MonoBehaviour
{

    public Transform indicator;
    public Transform head;
    public Vector2 offset;



    // Update is called once per frame
    void Update()
    {
        Vector3 temp = GetScreenPosition();
        indicator.localPosition = new Vector3(temp.x + offset.x, temp.y + offset.y, 0);
    }

    // get the current screen position of this display.
    protected Vector3 GetScreenPosition()
    {
        // convert the locations position to screen space using this locations system camera.
        Vector3 ScreenPosition = Camera.main.WorldToScreenPoint(head.position);
        ScreenPosition.x = (ScreenPosition.x / Camera.main.pixelWidth) + Camera.main.pixelRect.x;
        ScreenPosition.y = (ScreenPosition.y / Camera.main.pixelHeight) + Camera.main.pixelRect.y;
        ScreenPosition.z = head.position.z;

        Debug.Log(ScreenPosition);
        // return the resulting position.
        return ScreenPosition;
    }
}
