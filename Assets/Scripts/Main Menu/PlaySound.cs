﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class PlaySound : MonoBehaviour {
 public Slider volumeSlider;
 public AudioSource volumeAudio;
 void Start()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        audio.Play(44100);
    }

	 public void OnValueChanged (){
       volumeAudio.volume = volumeSlider.value;
 }
}
