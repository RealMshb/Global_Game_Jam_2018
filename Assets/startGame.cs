﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class startGame : MonoBehaviour
{

    public static startGame Instance;

    public GameObject DialogueBox;
    public GameObject PressSpace;
    public GameObject Collider;
    public Vector3 defaultCameraPosition;
    public Vector3 defaultCameraRotation;


    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        defaultCameraPosition = new Vector3(14.95f, 5.58f, -38.66f);
        defaultCameraRotation = new Vector3(45f, 225f, 0f);
        DialogueBox.SetActive(false);
        Collider.SetActive(false);
        PressSpace.SetActive(true);


    }

    // Update is called once per frame
    public bool isPressed = false;


    void Update()
    {
        if (Input.GetButton("start") && !isPressed)
        {

            //GameObject.FindWithTag("DialogueBox").GetComponent<Animator>().SetTrigger("Triggered");
            Invoke("startScene", 0.2f);

            //GameObject.FindWithTag("DialogueBox").GetComponent<Animator>().SetBool("IsOpen", false);
        }
    }

    void startScene()
    {

        isPressed = true;
        this.transform.position = defaultCameraPosition;
        //Vector3.SmoothDamp(transform.position, defaultCameraPosition ,ref velocity,1f)
        Collider.SetActive(true);
        DialogueBox.SetActive(true);
        PressSpace.SetActive(false);
        this.transform.eulerAngles = defaultCameraRotation;
        Camera.main.GetComponent<SwitchCharacter>().InititiateFirstCharacter();
    }
}
