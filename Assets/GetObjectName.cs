﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GetObjectName : MonoBehaviour {

	// Use this for initialization
	void Start () {
		this.GetComponent<Text>().text = this.transform.parent.parent.GetComponent<DialogueTrigger>().dialogue.name;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
